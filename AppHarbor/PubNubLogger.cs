﻿using PubNubMessaging.Core;
using SchemaTracker;
using System;
using System.Configuration;

namespace AppHarbor
{
    public class PubNubLogger : ILog
    {
        private const string ErrorText = "ERROR";
        private const string WarnText = "WARN";
        private const string InfoText = "INFO";

        private readonly Type _type;

        public PubNubLogger(Type type)
        {
            _type = type;
        }

        private string CreateLogMessage(string format, params object[] args)
        {
            return string.Format("[{0}] {1}", DateTime.Now.ToString("o"), string.Format(format, args));
        }

        public void Info(string format, params object[] args)
        {
            Console.WriteLine(CreateLogMessage(format, args), InfoText);
        }

        private void ErrorCallback(string s)
        {
            Console.WriteLine(s);
        }

        private void UserCallback(string s)
        {
            Console.WriteLine(s);
        }

        public void Warn(string format, params object[] args)
        {
            Console.WriteLine(CreateLogMessage(format, args), WarnText);
        }

        public void Error(Exception exception)
        {
            Console.WriteLine(CreateLogMessage(exception.ToString()), ErrorText);
        }

        public void Error(Exception exception, string format, params object[] args)
        {
            Console.WriteLine(CreateLogMessage(format + " - Exception = " + exception.ToString(), args), ErrorText);
        }
    }
}
