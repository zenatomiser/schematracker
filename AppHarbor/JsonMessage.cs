﻿using Newtonsoft.Json;

namespace AppHarbor
{
    public class JsonMessage
    {
        public enum AppCommand
        {
            Start = 0,
            Stop = 1,
            InitRepo = 2,
            NewService = 3,
            GitInfo = 4
        }
        [JsonProperty("command")]
        public AppCommand Command { get; set; }
        [JsonProperty("jsonObject", Required = Required.AllowNull)]
        public string JsonObject { get; set; }
    }
}
