﻿using Newtonsoft.Json;
using PubNubMessaging.Core;
using SchemaTracker;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace AppHarbor
{
    public class Program
    {
        private static Pubnub Pubnub;
        private static SchemaService ss;
        private static GitInfo gitInfo;
        private static string ApiKey;
        static void Main(string[] args)
        {
            var settingsReader = new AppSettingsReader();
            string pub = (string)settingsReader.GetValue("pub", typeof(String));
            string sub = (string)settingsReader.GetValue("sub", typeof(String));
            string sec = (string)settingsReader.GetValue("sec", typeof(String));
            ApiKey = (string)settingsReader.GetValue("api", typeof(String));
            Pubnub = new Pubnub(pub, sub, sec);

            string user = (string)settingsReader.GetValue("user", typeof(String));
            string pass = (string)settingsReader.GetValue("pass", typeof(String));
            string email = (string)settingsReader.GetValue("email", typeof(String));
            string repourl = (string)settingsReader.GetValue("repourl", typeof(String));
            string localurl = (string)settingsReader.GetValue("localurl", typeof(String));
            gitInfo = new GitInfo(user, pass, email, repourl, localurl);

            Pubnub.Subscribe<string>("requests", SubcriberMessage, SubcriberConnectedCallback, SubcriberEroorCallback);
            LogManager.GetLog = type => new PubNubLogger(type);

            ss = new SchemaService(ApiKey, gitInfo, null, 1800000);
            ss.Start();
            Console.ReadLine();
        }

        static void SubcriberMessage(string result)
        {
            var message = JsonConvert.DeserializeObject<List<object>>(result);
            if (message != null)
            {
                JsonMessage msg = JsonConvert.DeserializeObject<JsonMessage>(message[0].ToString());
                if (msg.Command == JsonMessage.AppCommand.Start)
                {
                    ss.Start();
                }
                if (msg.Command == JsonMessage.AppCommand.Stop)
                {
                    ss.Stop();
                }
                if (msg.Command == JsonMessage.AppCommand.InitRepo)
                {
                    string decryptedString = EnCryptDecrypt.CryptorEngine.Decrypt(msg.JsonObject, true);
                    gitInfo = JsonConvert.DeserializeObject<GitInfo>(decryptedString);

                    ss.NewRepo(gitInfo);
                }
                if (msg.Command == JsonMessage.AppCommand.NewService)
                {
                    ss = new SchemaService(ApiKey, gitInfo);
                }
                if (msg.Command == JsonMessage.AppCommand.GitInfo)
                {
                    string decryptedString = EnCryptDecrypt.CryptorEngine.Decrypt(msg.JsonObject, true);
                    gitInfo = JsonConvert.DeserializeObject<GitInfo>(decryptedString);
                }
            }
        }

        static void SubcriberConnectedCallback(string result)
        {
            Console.WriteLine(result);
        }
        static void SubcriberEroorCallback(string result)
        {
            Console.WriteLine(result);
        }
    }
}
